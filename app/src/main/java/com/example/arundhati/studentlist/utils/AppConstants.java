package com.example.arundhati.studentlist.utils;

/**
 * Created by Arundhati on 9/8/2015.
 */
public interface AppConstants {
    int VIEW = 0;
    int EDIT = 1;
    int DELETE = 2;
    String ROLL_NO = "Roll_No";
    String NAME = "Name";
    String ID_KEY = "rollNo";
    int EDIT_REQUEST_CODE = 100;
    int VIEW_REQUEST_CODE = 300;
    String EDIT_TEXT = "Edit Information";
    String DISPLAY_TEXT = "Display Information";
    String STUDENT_KEY = "Student";
    String IS_EDITABLE_KEY = "is Editable";
    int ENTER_NAME_ACTIVITY = 1;
    String DIALOG = "My Dialog";
    String NAME_KEY = "name";
    String ADDRESS_KEY = "address";
    String CONTACT_KEY = "mobile";
    String TEXT = "Student List";
    String TOAST_TEXT = "Enter Data";
    String NUMBER_TOAST_TEXT = "Enter 10 digit mobile number";
 }
