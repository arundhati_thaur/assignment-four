package com.example.arundhati.studentlist.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.example.arundhati.studentlist.R;
import com.example.arundhati.studentlist.entities.Student;
import com.example.arundhati.studentlist.utils.AppConstants;
import com.example.arundhati.studentlist.utils.MyDialogListener;

/**
 * Created by Arundhati on 9/7/2015.
 */
public class MyDialog extends DialogFragment implements AppConstants{
    MyDialogListener dialogListener;
    LayoutInflater inflater;
    View view;
    Student student;

    public Dialog onCreateDialog(Bundle savedInstanceState) {

        inflater = getActivity().getLayoutInflater();
        view = inflater.inflate(R.layout.dialog_layout, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        Button viewStudent = (Button) view.findViewById(R.id.viewStudent);
        Button edit = (Button) view.findViewById(R.id.edit);
        Button delete = (Button) view.findViewById(R.id.delete);
        viewStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogListener.onClick(VIEW);
                dismiss();
            }
        });
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogListener.onClick(EDIT);
                dismiss();
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogListener.onClick(DELETE);
                dismiss();
            }
        });

        builder.setView(view);
        return builder.create();
    }

    public void setDialogListener(MyDialogListener dialogListener) {
        this.dialogListener = dialogListener;
    }
}