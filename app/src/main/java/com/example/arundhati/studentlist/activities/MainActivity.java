package com.example.arundhati.studentlist.activities;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import com.example.arundhati.studentlist.Comparators.CompareByName;
import com.example.arundhati.studentlist.Comparators.CompareByRollNo;
import com.example.arundhati.studentlist.entities.Student;
import com.example.arundhati.studentlist.dialogs.MyDialog;
import com.example.arundhati.studentlist.R;
import com.example.arundhati.studentlist.adapters.MyAdapter;
import com.example.arundhati.studentlist.utils.AppConstants;
import com.example.arundhati.studentlist.utils.MyDialogListener;
import java.util.Collections;
import java.util.LinkedList;

public class MainActivity extends AppCompatActivity implements AppConstants, MyDialogListener, View.OnClickListener, AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {
    Intent intent,intentObj;
    int positionId;
    ListView list;
    GridView grid;
    Spinner spinner;
    Button addButton,listButton,gridButton;
    MyAdapter adapter;
    TextView textView;
    LinkedList<Student> studentData;
    ArrayAdapter<String> adapterSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialization();
        setListeners();


    }
    //function to initialize all the components
    public void initialization() {
        studentData = new LinkedList<>();
        addButton = (Button) findViewById(R.id.addButton);
        listButton = (Button) findViewById(R.id.listButton);
        gridButton = (Button) findViewById(R.id.gridButton);
        textView = (TextView) findViewById(R.id.textView);
        list = (ListView) findViewById(R.id.listView);
        grid = (GridView) findViewById(R.id.gridView);
        spinner = (Spinner) findViewById(R.id.spinner);
        adapter = new MyAdapter(studentData, this, R.layout.row_layout);
        list.setAdapter(adapter);
        adapterSpinner = new ArrayAdapter<>(this,android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.spinnerList));
        adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapterSpinner);
    }
    //function to set listeners on the components
    public void setListeners(){
        listButton.setOnClickListener(this);
        gridButton.setOnClickListener(this);
        addButton.setOnClickListener(this);
        list.setOnItemClickListener(this);
        spinner.setOnItemSelectedListener(this);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int rollNo = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (rollNo == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Student studentObj = new Student();

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case ENTER_NAME_ACTIVITY:
                    studentObj.name = data.getStringExtra(NAME_KEY);
                    studentObj.address = data.getStringExtra(ADDRESS_KEY);
                    studentObj.mobileNumber = Long.parseLong(data.getStringExtra(CONTACT_KEY));
                    studentObj.rollNo = Integer.parseInt(data.getStringExtra(ID_KEY));
                    textView.setText(TEXT);
                    studentData.add(studentObj);
                    if(spinner.getSelectedItem().toString().equals(NAME))
                    {
                        Collections.sort(studentData,new CompareByName());
                    }
                    adapter.notifyDataSetChanged();
                    adapterSpinner.notifyDataSetChanged();
                    break;

                case VIEW_REQUEST_CODE:
                    studentData.set(positionId, (Student) data.getSerializableExtra(STUDENT_KEY));

                    break;

                case EDIT_REQUEST_CODE:
                    studentData.set(positionId, (Student) data.getSerializableExtra(STUDENT_KEY));
                    adapter.notifyDataSetChanged();
                    adapterSpinner.notifyDataSetChanged();
                    break;


            }
        }
    }

    @Override
    public void onClick(int position) {
        switch (position) {
            case VIEW:
                intent = new Intent(this, EnterNameActivity.class);
                intent.putExtra(STUDENT_KEY, studentData.get(positionId));
                intent.putExtra(IS_EDITABLE_KEY, false);
                startActivityForResult(intent, VIEW_REQUEST_CODE);
                break;
            case EDIT:

                intent = new Intent(this, EnterNameActivity.class);
                intent.putExtra(STUDENT_KEY, studentData.get(positionId));
                intent.putExtra(IS_EDITABLE_KEY, true);
                startActivityForResult(intent, EDIT_REQUEST_CODE);
                break;
            case DELETE:
               studentData.remove(positionId);
                adapter.notifyDataSetChanged();
                break;

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.addButton:
                intentObj = new Intent(MainActivity.this, EnterNameActivity.class);
                startActivityForResult(intentObj, ENTER_NAME_ACTIVITY);
                break;

            case R.id.listButton:
                list.setVisibility(View.VISIBLE);
                grid.setVisibility(View.INVISIBLE);
                list.setAdapter(adapter);
                break;
            case R.id.gridButton:
                list.setVisibility(View.INVISIBLE);
                grid.setVisibility(View.VISIBLE);
                grid.setAdapter(adapter);
                grid.setOnItemClickListener(this);

}
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        MyDialog dialog = new MyDialog();
        dialog.setDialogListener(MainActivity.this);
        positionId = position;
        dialog.show(getFragmentManager(), DIALOG);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String sortBy = spinner.getSelectedItem().toString();
        if (sortBy.equals(ROLL_NO)) {
            Collections.sort(studentData, new CompareByRollNo());
            adapter.notifyDataSetChanged();
        } else if (sortBy.equals(NAME)) {
            Collections.sort(studentData, new CompareByName());
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}



