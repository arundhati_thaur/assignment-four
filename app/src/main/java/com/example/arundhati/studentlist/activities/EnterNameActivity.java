package com.example.arundhati.studentlist.activities;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.example.arundhati.studentlist.R;
import com.example.arundhati.studentlist.entities.Student;
import com.example.arundhati.studentlist.utils.AppConstants;

import org.w3c.dom.Text;

public class EnterNameActivity extends AppCompatActivity implements View.OnClickListener, AppConstants {
    EditText studentName = null;
    EditText studentContact = null;
    EditText studentAddress = null;
    TextView studentRollNo;
    Student student;
    Button saveButton;
    static int studentId = 1;

    boolean isEditable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_name);
        TextView textView = (TextView) findViewById(R.id.textView);
        studentRollNo = (TextView) findViewById(R.id.rollNo1);
        saveButton = (Button) findViewById(R.id.saveButton);
        final Button button2 = (Button) findViewById(R.id.cancelButton);
        studentName = (EditText) findViewById(R.id.editText1);
        studentContact = (EditText) findViewById(R.id.editText2);
        studentAddress = (EditText) findViewById(R.id.editText3);
        studentRollNo.setText(String.valueOf(studentId));
        saveButton.setOnClickListener(this);
        button2.setOnClickListener(this);
        if (getIntent().hasExtra(STUDENT_KEY)) {
            student = (Student) getIntent().getSerializableExtra(STUDENT_KEY);
            isEditable = getIntent().getBooleanExtra(IS_EDITABLE_KEY, true);
        }
        if (student != null) {
            studentName.setText(student.getName());
            studentContact.setText(String.valueOf(student.getMobileNumber()));
            studentAddress.setText(student.getAddress());
            studentRollNo.setText(String.valueOf(student.getRollNo()));
            studentName.setEnabled(isEditable);
            studentContact.setEnabled(isEditable);
            studentAddress.setEnabled(isEditable);
            if (!isEditable) {
                textView.setText(DISPLAY_TEXT);
                saveButton.setVisibility(View.INVISIBLE);
                studentName.setTextColor(Color.rgb(0, 0, 0));
                studentContact.setTextColor(Color.rgb(0, 0, 0));
                studentAddress.setTextColor(Color.rgb(0, 0, 0));
                studentRollNo.setTextColor(Color.rgb(0, 0, 0));

            } else {
                textView.setText(EDIT_TEXT);
                saveButton.setVisibility(View.VISIBLE);
            }
        }

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View v) {
        String name = studentName.getText().toString();
        String mobileNumber = studentContact.getText().toString();
        String address = studentAddress.getText().toString();
        String rollNo = String.valueOf(studentId);
        switch (v.getId()) {
            case R.id.saveButton:

                if (!name.isEmpty() && (!mobileNumber.isEmpty()) && (mobileNumber.length() == 10) && !address.isEmpty()) {
                    Intent intent = new Intent();
                    if (isEditable) {
                        student.setName(name);
                        student.setAddress(address);
                        student.setMobileNumber(Long.parseLong(mobileNumber));
                        intent.putExtra(STUDENT_KEY, student);

                    }
                    intent.putExtra(NAME_KEY, name);
                    intent.putExtra(CONTACT_KEY, mobileNumber);
                    intent.putExtra(ADDRESS_KEY, address);
                    intent.putExtra(ID_KEY,rollNo);
                    studentId++;
                    setResult(RESULT_OK, intent);
                    finish();
                } else if (name.isEmpty() || mobileNumber.isEmpty() || address.isEmpty()) {
                    Toast.makeText(this, TOAST_TEXT, Toast.LENGTH_SHORT).show();
                } else if (mobileNumber.length() < 10)
                    Toast.makeText(this, NUMBER_TOAST_TEXT, Toast.LENGTH_SHORT).show();
                break;

            case R.id.cancelButton:

                setResult(RESULT_CANCELED);
                finish();
                break;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);

    }
}
