package com.example.arundhati.studentlist.entities;

import java.io.Serializable;

/**
 * Created by Arundhati on 9/6/2015.
 */
public class Student implements Serializable
{
    public int rollNo;
    public long mobileNumber;
    public String name,address;

    public int getRollNo() {
        return rollNo;
    }

    public void setRollNo(int rollNo) {
        this.rollNo = rollNo;
    }

    public void setMobileNumber(long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public long getMobileNumber() {
        return mobileNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
