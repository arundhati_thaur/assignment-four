package com.example.arundhati.studentlist.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.arundhati.studentlist.R;
import com.example.arundhati.studentlist.entities.Student;

import java.util.List;

/**
 * Created by Arundhati on 9/4/2015.
 */
public class MyAdapter extends BaseAdapter {
    List<Student> data;
    Context context;
    int rowLayout;


    public MyAdapter(List<Student> data, Context context, int rowLayout) {
        this.data = data;
        this.context = context;
        this.rowLayout = rowLayout;
    }

    @Override
    public int getCount() {
        return data == null?0:data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //convertView  = LayoutInflater.from(context).inflate(rowLayout, null);
        convertView = View.inflate(context, rowLayout, null);
        TextView studentRollNo = (TextView) convertView.findViewById(R.id.studentRollNo);
        TextView studentName = (TextView) convertView.findViewById(R.id.name);
        TextView studentContact = (TextView) convertView.findViewById(R.id.mobile);
        TextView address = (TextView) convertView.findViewById(R.id.address);

        studentRollNo.setText(String.valueOf(data.get(position).getRollNo()));
        studentName.setText(data.get(position).getName());
        studentContact.setText(String.valueOf(data.get(position).getMobileNumber()));
        address.setText(data.get(position).getAddress());
        return convertView;
    }
}
