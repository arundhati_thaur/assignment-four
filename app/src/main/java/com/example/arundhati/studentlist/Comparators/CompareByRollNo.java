package com.example.arundhati.studentlist.Comparators;

import com.example.arundhati.studentlist.entities.Student;

import java.util.Comparator;

/**
 * Created by Arundhati on 9/9/2015.
 */
public class CompareByRollNo implements Comparator<Student> {
    @Override
    public int compare(Student lhs, Student rhs) {
        int rollNo1 = lhs.getRollNo();
        int rollNo2 = rhs.getRollNo();
        if(rollNo1 > rollNo2)
            return 1;
        else
            return -1;
    }
}
